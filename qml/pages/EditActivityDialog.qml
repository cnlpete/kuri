/*
 * Copyright (C) 2023 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQml 2.2
import QtQuick.Layouts 1.0

import Sailfish.Silica 1.0

import harbour.kuri 1.0

import "../tools/SharedResources.js" as SharedResources

Dialog {
    id: root
    allowedOrientations: Orientation.Portrait

    property var activity


    onDone: {
        if (result == DialogResult.Accepted) {
            // for some reason the description is empty when setting directly from the field without an additional cache;
            // since I do not know why, lets do it for all fields
            const name = nameField.text;
            const description = descriptionField.text;
            const activityType = SharedResources.arrayWorkoutTypes[activityTypeComboBox.currentIndex].name;

            ActivityHistory.updateActivitySummary(activity.activityId, activity.index, activity.activityType, activityType);

            activity.name = name;
            activity.description = description;
            activity.activityType = activityType;

            ActivityHistory.saveActivityChanges(activity.activityId, activity.index);
        }
    }

    DialogHeader {
        id: header
        title: qsTr("Edit")
        Layout.fillWidth: true
    }

    ColumnLayout {
        width: parent.width
        anchors.top: header.bottom
        anchors.bottom: parent.bottom

        spacing: Theme.paddingMedium

        TextField
        {
            id: nameField
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft

            label: qsTr("Name")
            placeholderText: qsTr("Name")
            text: activity.name

            focus: true
        }

        TextField
        {
            id: descriptionField
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft

            label: qsTr("Description")
            placeholderText: qsTr("Description")
            text: activity.description
        }

        RowLayout
        {
            Layout.fillWidth: true
            spacing: 0

            Item { width: Theme.horizontalPageMargin }

            Image
            {
                height: activityTypeComboBox.height
                width: height

                fillMode: Image.PreserveAspectFit

                source: SharedResources.arrayWorkoutTypes[activityTypeComboBox.currentIndex].icon
            }

            ComboBox
            {
                id: activityTypeComboBox
                Layout.fillWidth: true

                label: qsTr("Activity:")
                currentIndex: SharedResources.fncGetIndexByName(activity.activityType);
                menu: ContextMenu
                {
                    Repeater
                    {
                        model: SharedResources.arrayWorkoutTypes;
                        MenuItem { text: modelData.labeltext }
                    }
                }
            }

            Item { width: Theme.horizontalPageMargin }
        }

        Item { Layout.fillHeight: true }
    }
}
