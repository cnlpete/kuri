import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.WebView 1.0

Page {
    id: browser
    visible: false
    property url url: ""

    WebView {
        anchors.fill: parent
        active: true
        url: browser.url
    }
}
