/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.0
import Sailfish.Silica 1.0

import harbour.kuri 1.0

Page {
    id: page
    allowedOrientations: Orientation.Portrait

    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge;
        VerticalScrollDecorator {}
        Column
        {
            id: column
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader
            {
                title: qsTr("Map settings")
            }
            ComboBox
            {
                label: qsTr("Map center mode")
                menu: ContextMenu
                {
                    MenuItem { text: qsTr("Center current position on map") }
                    MenuItem { text: qsTr("Center track on map") }
                }
                Component.onCompleted: {
                    currentIndex = Settings.mapCenterMode;
                    currentIndexChanged.connect(function() {
                        Settings.mapCenterMode = currentIndex;
                    });
                }
            }
            Separator
            {
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }
            ComboBox
            {
                description: qsTr("Choose map style.")
                label: qsTr("Map")
                menu: ContextMenu {
                    MenuItem { text: qsTr("Streets"); onClicked: Settings.mapStyle = "mapbox://styles/mapbox/streets-v10" }
                    MenuItem { text: qsTr("Outdoors"); onClicked: Settings.mapStyle = "mapbox://styles/mapbox/outdoors-v10" }
                    MenuItem { text: qsTr("Light"); onClicked: Settings.mapStyle = "mapbox://styles/mapbox/light-v9" }
                    MenuItem { text: qsTr("Dark"); onClicked: Settings.mapStyle = "mapbox://styles/mapbox/dark-v9" }
                    MenuItem { text: qsTr("Satellite"); onClicked: Settings.mapStyle = "mapbox://styles/mapbox/satellite-v9" }
                    MenuItem { text: qsTr("Satellite Streets"); onClicked: Settings.mapStyle = "mapbox://styles/mapbox/satellite-streets-v10" }
                    MenuItem { text: qsTr("OSM Scout Server"); onClicked: Settings.mapStyle = "http://localhost:8553/v1/mbgl/style?style=osmbright" }
                }
                Component.onCompleted: {
                    switch (Settings.mapStyle) {
                        case "mapbox://styles/mapbox/streets-v10": currentIndex = 0; break;
                        case "mapbox://styles/mapbox/outdoors-v10": currentIndex = 1; break;
                        case "mapbox://styles/mapbox/light-v9": currentIndex = 2; break;
                        case "mapbox://styles/mapbox/dark-v9": currentIndex = 3; break;
                        case "mapbox://styles/mapbox/satellite-v9": currentIndex = 4; break;
                        case "mapbox://styles/mapbox/satellite-streets-v10": currentIndex = 5; break;
                        case "http://localhost:8553/v1/mbgl/style?style=osmbright": currentIndex = 6; break;
                        default: currentIndex = 1;
                    }
                }
            }
            Separator
            {
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }
            ComboBox
            {
                description: qsTr("Limiting tile caching ensures up-to-date maps and keeps disk use under control, but loads maps slower and causes more data traffic. " +
                                    "Note that the cache size settings will be applied after restart of the application.")
                label: qsTr("Cache size")
                menu: ContextMenu
                {
                    MenuItem { text: "25 MB"; onClicked: Settings.mapCache = 25 }
                    MenuItem { text: "50 MB"; onClicked: Settings.mapCache = 50 }
                    MenuItem { text: "100 MB"; onClicked: Settings.mapCache = 100 }
                    MenuItem { text: "250 MB"; onClicked: Settings.mapCache = 250 }
                }
                Component.onCompleted: {
                    switch (Settings.mapCache) {
                        case 25: currentIndex = 0; break;
                        case 50: currentIndex = 1; break;
                        case 100: currentIndex = 2; break;
                        case 250: currentIndex = 3; break;
                        default: currentIndex = 1;
                    }
                }
            }
        }
    }
}
