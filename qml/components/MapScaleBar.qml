/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQml 2.2

import Sailfish.Silica 1.0

Item {
    id: root
    height: base.height + text.height + text.anchors.bottomMargin
    opacity: 0.9
    visible: scaleWidth > 0

    property real   scaleWidth: 0
    property string text: ""

    Rectangle {
        id: base
        anchors.bottom: root.bottom
        color: "#333333"
        height: Math.floor(Theme.pixelRatio * 3)
        width: root.scaleWidth
    }

    Rectangle {
        anchors.bottom: base.top
        anchors.left: base.left
        color: "#333333"
        height: Math.floor(Theme.pixelRatio * 10)
        width: Math.floor(Theme.pixelRatio * 3)
    }

    Rectangle {
        anchors.bottom: base.top
        anchors.right: base.right
        color: "#333333"
        height: Math.floor(Theme.pixelRatio * 10)
        width: Math.floor(Theme.pixelRatio * 3)
    }

    Text {
        id: text
        anchors.bottom: base.top
        anchors.bottomMargin: Math.floor(Theme.pixelRatio * 4)
        anchors.horizontalCenter: base.horizontalCenter
        color: "black"
        font.bold: true
        font.family: "sans-serif"
        font.pixelSize: Math.round(Theme.pixelRatio * 18)
        horizontalAlignment: Text.AlignHCenter
        text: root.text
    }

    function siground(x, n) {
        // Round x to n significant digits.
        var mult = Math.pow(10, n - Math.floor(Math.log(x) / Math.LN10) - 1);
        return Math.round(x * mult) / mult;
    }

    function roundedDistace(dist) {
        // Return dist rounded to an even amount of user-visible units,
        // but keeping the value as meters.
        return siground(dist, 1);
    }

    // Update scalebar for current zoom level and latitude.
    function update() {
        const meters = map.metersPerPixel * map.width / 4;
        const dist = root.roundedDistace(meters);

        root.scaleWidth = dist / map.metersPerPixel;

        if (dist < 1000) {
            root.text = Math.ceil(dist).toString() + " m";
        } else {
            root.text = Math.ceil(dist / 1000.0).toString() + " km";
        }
    }
}
