/*
 * Copyright (C) 2021 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.5
import QtQuick.Layouts 1.1

import Sailfish.Silica 1.0

import harbour.kuri 1.0

ColumnLayout {
    property alias value: slider.value
    property alias minimumValue: slider.minimumValue
    property alias maximumValue: slider.maximumValue

    RowLayout {
        Layout.alignment: Qt.AlignCenter
        Layout.fillWidth: true

        spacing: Theme.paddingSmall

        Text {
            Layout.alignment: Qt.AlignBaseline

            text: qsTr("Pos:")
            font.pixelSize: Theme.fontSizeExtraSmall
            color: Theme.secondaryColor
        }

        Text {
            Layout.alignment: Qt.AlignBaseline

            text: "  " + StringFormatter.secondsToHoursMinutesSeconds(ActivityHistoryTrack.currentDuration)
            font.pixelSize: Theme.fontSizeExtraSmall
            color: Theme.primaryColor
        }

        Text {
            Layout.alignment: Qt.AlignBaseline

            text: "  " + (ActivityHistoryTrack.currentDistance/1000000.).toFixed(2)
            font.pixelSize: Theme.fontSizeExtraSmall
            color: Theme.primaryColor
        }
        Text {
            Layout.alignment: Qt.AlignBaseline

            text: qsTr("km")
            font.pixelSize: Theme.fontSizeTiny
            color: Theme.secondaryColor
        }

        Text {
            Layout.alignment: Qt.AlignBaseline

            text: "  " + StringFormatter.secondsToMinutesSeconds(ActivityHistoryTrack.currentPace)
            font.pixelSize: Theme.fontSizeExtraSmall
            color: Theme.primaryColor
        }
        Text {
            Layout.alignment: Qt.AlignBaseline

            text: qsTr("/km")
            font.pixelSize: Theme.fontSizeTiny
            color: Theme.secondaryColor
        }

        Text {
            Layout.alignment: Qt.AlignBaseline

            text: "  " + (ActivityHistoryTrack.currentHeartRate < 0.1 ? "-" : ActivityHistoryTrack.currentHeartRate.toFixed(1))
            font.pixelSize: Theme.fontSizeExtraSmall
            color: Theme.primaryColor
        }

        Text {
            Layout.alignment: Qt.AlignBaseline

            text: qsTr("bpm")
            font.pixelSize: Theme.fontSizeTiny
            color: Theme.secondaryColor
        }

        Text {
            Layout.alignment: Qt.AlignBaseline

            text: "  " + ActivityHistoryTrack.currentElevation.toFixed(0)
            font.pixelSize: Theme.fontSizeExtraSmall
            color: Theme.primaryColor
        }
        Text {
            Layout.alignment: Qt.AlignBaseline

            text: qsTr("m")
            font.pixelSize: Theme.fontSizeTiny
            color: Theme.secondaryColor
        }
    }

    RowLayout {
        Layout.alignment: Qt.AlignCenter
        Layout.fillWidth: true

        spacing: Theme.paddingSmall

        Slider {
            id: slider
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true

            minimumValue: 0
            maximumValue: 1

            function toLeft() {
                var currentValue = slider.value;
                if (currentValue > 0) {
                    slider.value = currentValue - 1;
                }
            }

            function toRight() {
                var currentValue = slider.value;
                if (currentValue < slider.maximumValue) {
                    slider.value = currentValue + 1;
                }
            }
        }

        Text
        {
            Layout.alignment: Qt.AlignCenter

            text: "  ⧼   "
            font.pixelSize: Theme.fontSizeLarge
            font.bold: true
            color: Theme.primaryColor

            MouseArea {
                id: mouseAreaLeft
                anchors.fill: parent
                preventStealing: true

                property bool longPress: false

                onClicked: {
                    longPress = false;
                    slider.toLeft();
                }
                onPressed: longPress = false
                onPressAndHold: longPress = true
            }
            Timer {
                id: timerLeft
                interval: 100
                running: mouseAreaLeft.pressed && mouseAreaLeft.longPress
                repeat: true
                onTriggered: slider.toLeft()
            }
            Rectangle {
                anchors.centerIn: parent
                width: parent.width * 1.5
                height: parent.height * 1.5
                color: mouseAreaLeft.pressed ? Theme.secondaryHighlightColor : "transparent"
                opacity: Theme.highlightBackgroundOpacity
                z: -1
            }
        }
        Text
        {
            Layout.alignment: Qt.AlignCenter

            text: "   ⧽  "
            font.pixelSize: Theme.fontSizeLarge
            font.bold: true
            color: Theme.primaryColor

            MouseArea {
                id: mouseAreaRight
                anchors.fill: parent
                preventStealing: true

                property bool longPress: false

                onClicked: {
                    longPress = false;
                    slider.toRight();
                }
                onPressed: longPress = false
                onPressAndHold: longPress = true
            }
            Timer {
                id: timerRight
                interval: 100
                running: mouseAreaRight.pressed && mouseAreaRight.longPress
                repeat: true
                onTriggered: slider.toRight()
            }
            Rectangle {
                anchors.centerIn: parent
                width: parent.width * 1.5
                height: parent.height * 1.5
                color: mouseAreaRight.pressed ? Theme.secondaryHighlightColor : "transparent"
                opacity: Theme.highlightBackgroundOpacity
                z: -1
            }
        }
        Item {
            width: Theme.paddingLarge
        }
    }
}
