// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use dirs::data_dir;
use once_cell::sync::Lazy;
use std::path::PathBuf;

pub static KURI_TRACK_DATA_DIR: Lazy<PathBuf> = Lazy::new(|| {
    const KURI_PATH: &'static str = "org.kuri/kuri/track/";
    let mut kuri_data_dir = data_dir().expect("Path to data dir");
    kuri_data_dir.push(KURI_PATH);
    kuri_data_dir
});

pub static KURI_META_DATA_DIR: Lazy<PathBuf> = Lazy::new(|| {
    const KURI_PATH: &'static str = "org.kuri/kuri/meta/";
    let mut kuri_data_dir = data_dir().expect("Path to data dir");
    kuri_data_dir.push(KURI_PATH);
    kuri_data_dir
});

pub static KURI_TMP_DATA_DIR: Lazy<PathBuf> = Lazy::new(|| {
    const KURI_PATH: &'static str = "org.kuri/kuri/tmp/";
    let mut kuri_data_dir = data_dir().expect("Path to data dir");
    kuri_data_dir.push(KURI_PATH);
    kuri_data_dir
});
