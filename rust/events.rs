// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

#[cxx::bridge(namespace = "kuri::rs")]
pub mod ffi {
    #[namespace = "kuri::ffi"]
    unsafe extern "C++" {
        include!("kuri-rs/ffi/events.h");

        type EventVoid;
        pub fn new_event_void() -> UniquePtr<EventVoid>;
        pub fn notify(self: Pin<&mut EventVoid>);

        type EventBool;
        pub fn new_event_bool() -> UniquePtr<EventBool>;
        pub fn notify(self: Pin<&mut EventBool>, value: bool);

        type EventU8;
        pub fn new_event_u8() -> UniquePtr<EventU8>;
        pub fn notify(self: Pin<&mut EventU8>, value: u8);

        type EventU16;
        pub fn new_event_u16() -> UniquePtr<EventU16>;
        pub fn notify(self: Pin<&mut EventU16>, value: u16);

        type EventU32;
        pub fn new_event_u32() -> UniquePtr<EventU32>;
        pub fn notify(self: Pin<&mut EventU32>, value: u32);

        type EventU64;
        pub fn new_event_u64() -> UniquePtr<EventU64>;
        pub fn notify(self: Pin<&mut EventU64>, value: u64);

        type EventI8;
        pub fn new_event_i8() -> UniquePtr<EventI8>;
        pub fn notify(self: Pin<&mut EventI8>, value: i8);

        type EventI16;
        pub fn new_event_i16() -> UniquePtr<EventI16>;
        pub fn notify(self: Pin<&mut EventI16>, value: i16);

        type EventI32;
        pub fn new_event_i32() -> UniquePtr<EventI32>;
        pub fn notify(self: Pin<&mut EventI32>, value: i32);

        type EventI64;
        pub fn new_event_i64() -> UniquePtr<EventI64>;
        pub fn notify(self: Pin<&mut EventI64>, value: i64);

        type EventF32;
        pub fn new_event_f32() -> UniquePtr<EventF32>;
        pub fn notify(self: Pin<&mut EventF32>, value: f32);

        type EventF64;
        pub fn new_event_f64() -> UniquePtr<EventF64>;
        pub fn notify(self: Pin<&mut EventF64>, value: f64);
    }
}
