// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

mod activity;
pub use crate::activity_history::activity::Activity;

mod track;
pub use crate::activity_history::track::Track;

use crate::events;
use crate::paths::{KURI_META_DATA_DIR, KURI_TRACK_DATA_DIR};

use crossbeam_channel::{bounded, unbounded, Receiver, Sender};

use std::collections::hash_map::HashMap;
use std::thread::{self, JoinHandle};

pub use ffi::{
    ActivityHistory, ActivitySummary, ArtifactAndHistoryEvents, ArtifactEvent, HistoryEvents,
};

#[derive(Debug)]
enum Command {
    LoadAllActivities,
    LoadActivity(String),
    LoadTrack(String),
}

#[derive(Debug)]
enum Artifact {
    Activity(Box<Activity>),
    ActivityLoadingFinished,
    Track(Box<Track>),
    TrackLoadingFailed,
}

pub struct ActivityHistoryStore {
    worker_queue: Option<Sender<Command>>,
    worker_th_jh: Option<JoinHandle<()>>,
    artifacts: Receiver<Artifact>,
    loading_finished: bool,
    activities: Vec<Box<Activity>>,
    activity_summary: HashMap<String, ActivitySummary>,
    track: Option<Box<Track>>,
}

impl Drop for ActivityHistoryStore {
    fn drop(&mut self) {
        self.worker_queue = None;
        self.worker_th_jh.take().map(|jh| {
            jh.join()
                .map_err(|e| {
                    println!("Error joining worker thread: {:?}", e);
                    e
                })
                .ok()
        });
    }
}

pub fn new_activity_history(events: ArtifactAndHistoryEvents) -> ActivityHistory {
    ActivityHistory::new(events)
}

impl ActivityHistory {
    pub fn new(events: ArtifactAndHistoryEvents) -> Self {
        let (sender, receiver) = bounded(100);
        let (outbox, inbox) = unbounded();
        let worker_th_jh = {
            let artifact_event = events.artifact;
            thread::spawn(move || {
                Self::command_handler(receiver, outbox, artifact_event);
            })
        };
        Self {
            store: Box::new(ActivityHistoryStore {
                worker_queue: Some(sender),
                worker_th_jh: Some(worker_th_jh),
                artifacts: inbox,
                loading_finished: false,
                activities: Vec::new(),
                activity_summary: HashMap::new(),
                track: None,
            }),
            events: events.history,
        }
    }

    fn command_handler(
        receiver: Receiver<Command>,
        outbox: Sender<Artifact>,
        mut event: ArtifactEvent,
    ) {
        let deliver_activity = |outbox: &Sender<Artifact>, event: &mut ArtifactEvent, activity| {
            outbox
                .send(Artifact::Activity(Box::new(activity)))
                .map_err(|e| println!("Could not send Activity: {}", e))
                .ok();
            event.new_artifacts.pin_mut().notify();
        };

        while let Ok(cmd) = receiver.recv() {
            match cmd {
                Command::LoadAllActivities => {
                    Activity::read_all_activities(|activity| {
                        deliver_activity(&outbox, &mut event, activity);
                    })
                    .map_err(|e| println!("Could not load activities: {}", e))
                    .ok();
                    outbox
                        .send(Artifact::ActivityLoadingFinished)
                        .map_err(|e| println!("Could not send activity loading finished: {}", e))
                        .ok();
                    event.new_artifacts.pin_mut().notify();
                }
                Command::LoadActivity(id) => {
                    Activity::read_activity_by_id(&id)
                        .map(|activity| {
                            deliver_activity(&outbox, &mut event, activity);
                        })
                        .map_err(|e| println!("Could not read activity meta data: {}", e))
                        .ok();
                    event.new_artifacts.pin_mut().notify();
                }
                Command::LoadTrack(id) => {
                    Track::read_track(id)
                        .map_or_else(
                            |_| outbox.send(Artifact::TrackLoadingFailed),
                            |track| outbox.send(Artifact::Track(Box::new(track))),
                        )
                        .map_err(|e| println!("Could not read track: {}", e))
                        .ok();

                    event.new_artifacts.pin_mut().notify();
                }
            }
        }
    }

    pub fn shutdown(&mut self) {
        self.store.worker_queue = None;
    }

    pub fn process_artifacts(&mut self) {
        let old_activity_total_count = self.activity_total_count();
        while let Ok(artifact) = self.store.artifacts.try_recv() {
            match artifact {
                Artifact::Activity(a) => {
                    for &k in &["allactivities", a.activity_type()] {
                        let s = self.store.activity_summary.entry(k.into()).or_default();
                        s.activity_count += 1;
                        s.distance += a.distance();
                        s.duration += a.duration_active();
                    }
                    self.store.activities.push(a);
                }
                Artifact::ActivityLoadingFinished => {
                    self.store.loading_finished = true;
                    self.events.activities_loading.pin_mut().notify(false);

                    let activity_total_count = self.activity_total_count() as u64;
                    self.events
                        .activity_total_count_changed
                        .pin_mut()
                        .notify(activity_total_count);
                    self.events.activity_summary_changed.pin_mut().notify();
                }
                Artifact::Track(t) => {
                    self.store.track = Some(t);
                    self.events.track_available.pin_mut().notify(true);
                }
                Artifact::TrackLoadingFailed => {
                    self.events.track_available.pin_mut().notify(false);
                }
            }
        }

        let new_activity_total_count = self.activity_total_count();
        if self.store.loading_finished && new_activity_total_count != old_activity_total_count {
            self.events
                .activity_total_count_changed
                .pin_mut()
                .notify(new_activity_total_count as u64);
            self.events.activity_summary_changed.pin_mut().notify();
        }
    }

    fn send(&mut self, cmd: Command) {
        let _ = self
            .store
            .worker_queue
            .as_mut()
            .map(|sender| sender.send(cmd));
    }

    pub fn load_all_activities(&mut self) {
        self.store.activities.clear();
        self.events.activities_loading.pin_mut().notify(true);
        self.send(Command::LoadAllActivities);
    }

    pub fn load_activity(&mut self, id: &str) {
        self.send(Command::LoadActivity(id.into()));
    }

    pub fn delete_activity(&mut self, id: &str) {
        self.store
            .activities
            .as_slice()
            .into_iter()
            .position(|a| a.id == id)
            .map(|index| {
                let a = self.store.activities.remove(index);
                for &k in &["allactivities", a.activity_type()] {
                    self.store.activity_summary.get_mut(k.into()).map(|s| {
                        s.activity_count = s.activity_count.saturating_sub(1);
                        s.duration = s.duration.saturating_sub(a.duration_active());
                        s.distance = s.distance.saturating_sub(a.distance());
                    });
                }
                self.events.activity_summary_changed.pin_mut().notify();

                let mut path = KURI_META_DATA_DIR.clone();
                path.push(format!("{}.meta.toml", id));
                std::fs::remove_file(path)
                    .map_err(|e| println!("Error discarding recording: {}", e))
                    .ok();
                let mut path = KURI_TRACK_DATA_DIR.clone();
                path.push(format!("{}.track-raw.toml.zst", id));
                std::fs::remove_file(path)
                    .map_err(|e| println!("Error discarding recording: {}", e))
                    .ok();
            });

        let activity_total_count = self.activity_total_count() as u64;
        self.events
            .activity_total_count_changed
            .pin_mut()
            .notify(activity_total_count);
    }

    pub fn load_track(&mut self, id: &str) {
        self.events.track_available.pin_mut().notify(false);
        self.send(Command::LoadTrack(id.into()));
    }

    pub fn discard_track(&mut self) {
        self.store.track = None;
        self.events.track_available.pin_mut().notify(false);
    }

    pub fn activity_total_count(&self) -> u64 {
        self.store.activities.len() as u64
    }

    pub fn activity_type_count(&self) -> u64 {
        // ignore the 'allactivities' key
        (self.store.activity_summary.len() as u64).saturating_sub(1)
    }

    pub fn activity_at(&self, index: u64) -> &Activity {
        &self.store.activities[index as usize]
    }

    pub fn activity_at_mut(&mut self, index: u64) -> &mut Activity {
        &mut self.store.activities[index as usize]
    }

    fn activity_summary(&self, activity_type: &str) -> ActivitySummary {
        self.store
            .activity_summary
            .get(activity_type.into())
            .map(|s| *s)
            .unwrap_or(ActivitySummary::default())
    }

    fn update_activity_summary(
        &mut self,
        activity: &Activity,
        activity_type_old: &str,
        activity_type_new: &str,
    ) {
        self.store
            .activity_summary
            .get_mut(activity_type_old.into())
            .map(|s| {
                s.activity_count = s.activity_count.saturating_sub(1);
                s.duration = s.duration.saturating_sub(activity.duration_active());
                s.distance = s.distance.saturating_sub(activity.distance());
            });

        let s = self
            .store
            .activity_summary
            .entry(activity_type_new.into())
            .or_default();
        s.activity_count += 1;
        s.distance += activity.distance();
        s.duration += activity.duration_active();

        self.events.activity_summary_changed.pin_mut().notify();
    }

    pub fn track(&self) -> *const Track {
        self.store
            .track
            .as_ref()
            .map(|t| &**t as *const Track)
            .unwrap_or(std::ptr::null())
    }
}

pub fn new_artifact_and_history_events() -> ArtifactAndHistoryEvents {
    ArtifactAndHistoryEvents {
        artifact: ArtifactEvent {
            new_artifacts: events::ffi::new_event_void(),
        },
        history: HistoryEvents {
            activities_loading: events::ffi::new_event_bool(),
            track_available: events::ffi::new_event_bool(),
            activity_total_count_changed: events::ffi::new_event_u64(),
            activity_summary_changed: events::ffi::new_event_void(),
        },
    }
}

unsafe impl Send for ArtifactEvent {}

#[cxx::bridge(namespace = "kuri::rs")]
mod ffi {
    pub struct ActivityHistory {
        store: Box<ActivityHistoryStore>,
        events: HistoryEvents,
    }

    struct ArtifactEvent {
        pub new_artifacts: UniquePtr<EventVoid>,
    }

    struct HistoryEvents {
        pub activities_loading: UniquePtr<EventBool>,
        pub track_available: UniquePtr<EventBool>,
        pub activity_total_count_changed: UniquePtr<EventU64>,
        pub activity_summary_changed: UniquePtr<EventVoid>,
    }

    pub struct ArtifactAndHistoryEvents {
        pub artifact: ArtifactEvent,
        pub history: HistoryEvents,
    }

    #[derive(Debug, Default, Clone, Copy)]
    pub struct Measurement {
        pub timestamp: i64,
        pub latitude: f64,
        pub longitude: f64,
        pub altitude: f32,
        pub horizontal_accuracy: u16,
        pub vertical_accuracy: u16,
        pub position_valid: bool,
        pub heart_rate: u16,
    }

    #[derive(Debug, Default, Clone, Copy)]
    struct ActivitySummary {
        activity_count: u64,
        duration: u64,
        distance: u64,
    }

    extern "Rust" {
        type ActivityHistoryStore;
        type Activity;
        type Track;

        fn new_artifact_and_history_events() -> ArtifactAndHistoryEvents;

        fn new_activity_history(events: ArtifactAndHistoryEvents) -> ActivityHistory;

        fn shutdown(self: &mut ActivityHistory);

        fn process_artifacts(self: &mut ActivityHistory);

        fn load_all_activities(self: &mut ActivityHistory);
        fn load_activity(self: &mut ActivityHistory, id: &str);
        fn delete_activity(self: &mut ActivityHistory, id: &str);
        fn load_track(self: &mut ActivityHistory, id: &str);
        fn discard_track(self: &mut ActivityHistory);

        fn activity_total_count(self: &ActivityHistory) -> u64;
        fn activity_type_count(self: &ActivityHistory) -> u64;
        fn activity_at(self: &ActivityHistory, index: u64) -> &Activity;
        fn activity_at_mut(self: &mut ActivityHistory, index: u64) -> &mut Activity;
        fn activity_summary(self: &ActivityHistory, activity_type: &str) -> ActivitySummary;
        fn update_activity_summary(
            self: &mut ActivityHistory,
            activity: &Activity,
            activity_type_old: &str,
            activity_type_new: &str,
        );
        fn track(self: &ActivityHistory) -> *const Track;

        fn id(self: &Activity) -> &str;
        fn start_time(self: &Activity) -> &str;
        fn activity_type(self: &Activity) -> &str;
        fn name(self: &Activity) -> &str;
        fn description(self: &Activity) -> &str;
        fn duration_active(self: &Activity) -> u64;
        fn duration_pause(self: &Activity) -> u64;
        fn distance(self: &Activity) -> u64;
        fn speed_average(self: &Activity) -> f64;
        fn pace_average(self: &Activity) -> f64;
        fn heart_rate_average(self: &Activity) -> u16;

        fn save_changes(self: &mut Activity);
        fn set_activity_type(self: &mut Activity, activity_type: &str);
        fn set_activity_name(self: &mut Activity, activity_name: &str);
        fn set_activity_description(self: &mut Activity, activity_description: &str);

        fn id(self: &Track) -> &str;
        fn total_point_count(self: &Track) -> u64;
        fn section_count(self: &Track) -> u64;
        fn section_point_count(self: &Track, section: u64) -> u64;
        fn measurement_at(self: &Track, section: u64, index: u64) -> Measurement;
    }
    #[namespace = "kuri::ffi"]
    unsafe extern "C++" {
        include!("kuri-rs/ffi/events.h");
        type EventVoid = crate::events::ffi::EventVoid;
        type EventBool = crate::events::ffi::EventBool;
        type EventU64 = crate::events::ffi::EventU64;
    }
}
