// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use super::fusion::{Fusion, RecordingState, Tag, TaggedMeasurement};
use super::storage::{FileName, Storage, CURRENT_RECORDING_DATA_FILE_NAME};

use chrono::{Local, TimeZone};

use std::io::{BufWriter, Write};

pub(super) struct DataWriter<S: Storage> {
    data_file: Option<BufWriter<S>>,
    header_written: bool,
    timestamp_last_stored_measurement: i64,
}

impl<S: Storage<Backend = S>> DataWriter<S> {
    pub fn new() -> Self {
        Self {
            data_file: None,
            header_written: false,
            timestamp_last_stored_measurement: 0,
        }
    }

    pub fn save_recording(&mut self, file_id: &str) {
        let file_name = format!("{}.track-raw.toml.zst", file_id);
        S::make_persistent_compressed(
            CURRENT_RECORDING_DATA_FILE_NAME,
            FileName::TrackData(&file_name),
        );

        *self = Self::new();
    }

    pub fn discard_recording(&mut self) {
        S::discard(CURRENT_RECORDING_DATA_FILE_NAME);

        *self = Self::new();
    }

    pub fn start(&mut self, fusion: &Fusion) {
        if fusion.recording_state() != RecordingState::Idle {
            println!("Error! Starting when not in Idle state!");
            return;
        }

        self.data_file = S::new_tmp_storage(CURRENT_RECORDING_DATA_FILE_NAME);

        self.data_file.as_mut().map(|buf| {
            writeln!(buf, "[info]")
                .map_err(|e| println!("Error occured: {:?}", e))
                .ok();
            writeln!(buf, "version = {}", 1)
                .map_err(|e| println!("Error occured: {:?}", e))
                .ok();
        });
    }

    pub fn stop(&mut self, fusion: &Fusion) -> Option<S> {
        if !self.header_written {
            return None;
        }

        let measurements = fusion.measurements(self.timestamp_last_stored_measurement);

        for m in measurements {
            self.store_measurement(m);
        }

        self.data_file.take().and_then(|buf| buf.into_inner().ok())
    }

    pub fn process(&mut self, fusion: &Fusion) {
        let measurements = fusion.measurements(self.timestamp_last_stored_measurement);

        if !self.header_written {
            measurements.first().map(|m| {
                let start_time = Local.timestamp(m.measurement.timestamp, 0).format("%+");
                self.data_file.as_mut().map(|buf| {
                    writeln!(buf, "start-time = '{}'", start_time)
                        .map_err(|e| println!("Error occured: {:?}", e))
                        .ok();
                    writeln!(buf, "")
                        .map_err(|e| println!("Error occured: {:?}", e))
                        .ok();
                });

                self.header_written = true;
            });
        }

        for m in measurements {
            if m.pending {
                break;
            } else if m.about_to_drop {
                self.store_measurement(m);
            } else if m.measurement.position.is_some()
                && m.measurement.heart_rate.is_some()
                && !m.heart_rate_from_cache
            {
                self.store_measurement(m);
            } else {
                break;
            }
        }
    }

    fn store_measurement(&mut self, m: &TaggedMeasurement) {
        self.timestamp_last_stored_measurement = m.measurement.timestamp;

        let write_measurement_to_file = |buf: &mut BufWriter<S>, measurement| {
            writeln!(buf, "[[section.measurement]]")
                .map_err(|e| println!("Error occured: {:?}", e))
                .ok();
            writeln!(buf, "{}", toml::to_string_pretty(measurement).unwrap())
                .map_err(|e| println!("Error occured: {:?}", e))
                .ok();
        };

        if m.tag == Tag::SectionCrossing {
            self.data_file
                .as_mut()
                .map(|buf| write_measurement_to_file(buf, &m.measurement));
        }

        if m.tag == Tag::SectionStart || m.tag == Tag::SectionCrossing {
            self.data_file.as_mut().map(|buf| {
                writeln!(buf, "[[section]]")
                    .map_err(|e| println!("Error occured: {:?}", e))
                    .ok();
            });
        }

        self.data_file
            .as_mut()
            .map(|buf| write_measurement_to_file(buf, &m.measurement));
    }
}

#[cfg(test)]
mod tests;
