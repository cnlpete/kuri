// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use super::PositionInfo;

use crate::activity_recorder::aggregator::ActivityMetaData;
use crate::records::{Measurement, Position};

use chrono::{Local, TimeZone};

use std::collections::VecDeque;

impl From<PositionInfo> for Position {
    fn from(position_info: PositionInfo) -> Self {
        Self {
            latitude: position_info.latitude,
            longitude: position_info.longitude,
            altitude: position_info.altitude,
            horizontal_accuracy: position_info.horizontal_accuracy,
            vertical_accuracy: position_info.vertical_accuracy,
        }
    }
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub(super) enum RecordingState {
    Idle,
    Active,
    Pause,
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub(super) enum Tag {
    Idle,
    Active,
    SectionStart,
    SectionCrossing,
}

#[derive(Copy, Clone)]
pub(super) struct TaggedMeasurement {
    pub tag: Tag,
    /// placeholder measurement which contains only a timestamp and no further information;
    /// without measurement this is only true on a pause/resume/section crossing to process the timestamp
    pub is_placeholder: bool,
    /// the latest two measurements have the pending flag due to the chance of a pause/resume/section crossing
    pub pending: bool,
    /// the measurement will be dropped with the next tick and needs to be processed
    pub about_to_drop: bool,
    /// the heart rate will be cached for up to three ticks; this flag indicates if the current value is cached
    pub heart_rate_from_cache: bool,
    pub measurement: Measurement,
}

impl TaggedMeasurement {
    fn new(timestamp: i64) -> Self {
        Self {
            tag: Tag::Idle,
            is_placeholder: true,
            pending: false,
            about_to_drop: false,
            heart_rate_from_cache: false,
            measurement: Measurement {
                timestamp,
                position: None,
                heart_rate: None,
            },
        }
    }
}

pub(super) struct Fusion {
    timestamp_recording_start: i64,
    timestamp_section_start: i64,
    recording_start_pending: bool,
    recording_state: RecordingState,
    timestamp_last_pause_start: i64,
    timestamp_last_heart_rate_update: i64,
    measurement_buffer: VecDeque<TaggedMeasurement>,
}

impl Fusion {
    pub fn new() -> Self {
        Self {
            timestamp_recording_start: 0,
            timestamp_section_start: 0,
            recording_start_pending: false,
            recording_state: RecordingState::Idle,
            timestamp_last_pause_start: 0,
            timestamp_last_heart_rate_update: 0,
            measurement_buffer: VecDeque::with_capacity(10),
        }
    }

    pub fn reset(&mut self) {
        *self = Self::new();
    }

    pub fn start(&mut self, timestamp: i64) {
        if self.recording_state != RecordingState::Idle {
            println!("Error! Starting when not in Idle state!");
            return;
        }

        // set the recording start timestamp in order to ignore measurements done before start in case there is a delayed delivery of the measurements
        self.timestamp_recording_start = timestamp;
        self.recording_start_pending = true;
    }

    pub fn stop(&mut self, timestamp: i64, amd: &mut ActivityMetaData) {
        self.recording_start_pending = false;

        match self.recording_state {
            RecordingState::Idle => {
                return;
            }
            RecordingState::Pause => {}
            RecordingState::Active => {
                // create measurement at end of section
                self.measurement_from_buffer(timestamp)
                    .map(|m| m.is_placeholder = false);
            }
        }

        self.measurement_buffer
            .iter_mut()
            .filter(|m| m.measurement.timestamp > timestamp)
            .for_each(|m| m.tag = Tag::Idle);

        self.recording_state = RecordingState::Idle;
        amd.recording.set(false);
    }

    pub fn pause(&mut self, timestamp: i64, amd: &mut ActivityMetaData) {
        match self.recording_state {
            RecordingState::Active => {
                amd.pausing.set(true);
                self.timestamp_last_pause_start = timestamp;

                for m in self.measurement_buffer.iter_mut() {
                    if m.measurement.timestamp > timestamp {
                        m.tag = Tag::Idle;
                    }
                }

                self.measurement_from_buffer(timestamp)
                    .map(|m| m.is_placeholder = false);
                self.recording_state = RecordingState::Pause;
            }
            _ => return,
        }
    }

    pub fn resume(&mut self, timestamp: i64, amd: &mut ActivityMetaData) {
        match self.recording_state {
            RecordingState::Pause => {
                amd.pausing.set(false);

                let stats = &mut amd.rec.statistics;
                stats.duration_pause.set(
                    stats.duration_pause.peek()
                        + (timestamp - self.timestamp_last_pause_start) as u64,
                );

                for m in self.measurement_buffer.iter_mut() {
                    if m.measurement.timestamp > timestamp {
                        m.tag = Tag::Active;
                    } else if m.measurement.timestamp == timestamp {
                        if m.tag == Tag::Active {
                            m.tag = Tag::SectionCrossing;
                        } else {
                            m.tag = Tag::Active;
                        }
                    }
                }

                self.measurement_from_buffer(timestamp).map(|m| {
                    m.is_placeholder = false;
                    if m.tag != Tag::SectionCrossing {
                        m.tag = Tag::SectionStart;
                    }
                });
                self.recording_state = RecordingState::Active;
                self.next_section(timestamp, amd);
            }
            _ => return,
        }
    }

    pub fn next_section(&mut self, timestamp: i64, amd: &mut ActivityMetaData) {
        match self.recording_state {
            RecordingState::Active => {
                if self.timestamp_section_start == timestamp {
                    return;
                }
                self.timestamp_section_start = timestamp;

                amd.section_count.set(amd.section_count.peek() + 1);
                amd.duration_in_section.set(0);

                self.measurement_from_buffer(timestamp).map(|m| {
                    m.is_placeholder = false;
                    if m.tag != Tag::SectionStart {
                        m.tag = Tag::SectionCrossing;
                    }
                });
            }
            _ => {}
        }
    }

    fn measurement_from_buffer(&mut self, timestamp: i64) -> Option<&mut TaggedMeasurement> {
        if self.measurement_buffer.is_empty() {
            let mut m = TaggedMeasurement::new(timestamp);
            if self.recording_state == RecordingState::Active {
                m.tag = Tag::Active;
            }
            self.measurement_buffer.push_back(m);
            return self.measurement_buffer.back_mut();
        }

        if let Some(oldest) = self.measurement_buffer.front() {
            if timestamp < oldest.measurement.timestamp {
                return None;
            }
        }

        if let Some(latest) = self.measurement_buffer.back() {
            if latest.measurement.timestamp < timestamp {
                for ts in latest.measurement.timestamp..=timestamp {
                    let mut m = TaggedMeasurement::new(ts);
                    if self.recording_state == RecordingState::Active {
                        m.tag = Tag::Active;
                    }
                    self.measurement_buffer.push_back(m);
                }

                return self.measurement_buffer.back_mut();
            }
        }

        for m in self.measurement_buffer.iter_mut() {
            if m.measurement.timestamp == timestamp {
                return Some(m);
            }
        }

        None
    }

    pub fn position_update(
        &mut self,
        _timestamp: i64,
        position_info: PositionInfo,
        amd: &mut ActivityMetaData,
    ) {
        // the delivery of the GPS measurement can be delayed -> use the measurement timestamp
        let timestamp = position_info.timestamp;
        amd.elevation
            .set((position_info.altitude * 1000.).round() as i64);

        if timestamp < self.timestamp_recording_start {
            return;
        }

        let heart_rate = *amd.heart_rate.peek();
        let heart_rate = if heart_rate != 0 && timestamp - self.timestamp_last_heart_rate_update < 3
        {
            Some(heart_rate)
        } else {
            None
        };

        self.measurement_from_buffer(timestamp).map(|m| {
            m.is_placeholder = false;
            if m.measurement.position.is_none() {
                m.measurement.position = Some(position_info.into());
                if m.measurement.heart_rate.is_none() {
                    m.measurement.heart_rate = heart_rate;
                    m.heart_rate_from_cache = true;
                }
            }
        });

        self.process(timestamp, amd);
    }

    pub fn heart_rate_update(
        &mut self,
        timestamp: i64,
        heart_rate: u16,
        amd: &mut ActivityMetaData,
    ) {
        if timestamp < self.timestamp_recording_start {
            return;
        }

        self.timestamp_last_heart_rate_update = timestamp;
        amd.heart_rate.set(heart_rate);
        self.measurement_from_buffer(timestamp).map(|m| {
            m.is_placeholder = false;
            if !(m.measurement.heart_rate.is_some() && !m.heart_rate_from_cache) {
                m.measurement.heart_rate = Some(heart_rate);
                m.heart_rate_from_cache = false;
            }
        });

        self.process(timestamp, amd);
    }

    pub fn tick(&mut self, timestamp: i64, amd: &mut ActivityMetaData) {
        self.measurement_from_buffer(timestamp);
        self.process(timestamp, amd);
    }

    fn process(&mut self, timestamp: i64, amd: &mut ActivityMetaData) {
        if timestamp < self.timestamp_recording_start {
            return;
        }

        if self.recording_start_pending {
            self.recording_start_pending = false;
            amd.recording.set(true);
            self.timestamp_recording_start = timestamp;
            self.timestamp_section_start = timestamp;
            self.recording_state = RecordingState::Active;
            // create measurement at section start
            self.measurement_from_buffer(timestamp).map(|m| {
                m.is_placeholder = false;
                m.tag = Tag::SectionStart;
            });
            amd.rec.info.start_time = Local.timestamp(timestamp, 0).format("%+").to_string();
        }

        if *amd.recording.peek() && !*amd.pausing.peek() {
            let stats = &mut amd.rec.statistics;
            let new_duration =
                (timestamp - self.timestamp_recording_start) as u64 - stats.duration_pause.peek();
            if *stats.duration_active.peek() != new_duration {
                stats.duration_active.set(new_duration);
            }

            let new_duration_in_section = (timestamp - self.timestamp_section_start) as u64;
            if *amd.duration_in_section.peek() != new_duration_in_section {
                amd.duration_in_section.set(new_duration_in_section);
            }
        }

        for (i, m) in self.measurement_buffer.iter_mut().rev().enumerate() {
            // set the latest two measurement to pending in case of pause/resume/section crossing
            if i < 2 {
                m.pending = true;
            } else if m.pending {
                m.pending = false;
            } else {
                break;
            }
        }

        while let Some(m) = self.measurement_buffer.front() {
            if m.about_to_drop {
                let _ = self.measurement_buffer.pop_front();
            } else {
                break;
            }
        }

        self.measurement_buffer
            .iter_mut()
            .rev()
            .skip(9)
            .for_each(|m| m.about_to_drop = true);
    }

    pub fn recording_state(&self) -> RecordingState {
        self.recording_state
    }

    pub fn timestamp_recording_start(&self) -> i64 {
        self.timestamp_recording_start
    }

    pub fn measurements(&self, timestamp_last_consumed: i64) -> Vec<&TaggedMeasurement> {
        self.measurement_buffer
            .iter()
            .filter(|m| {
                m.measurement.timestamp > timestamp_last_consumed
                    && m.tag != Tag::Idle
                    && !m.is_placeholder
            })
            .map(|m| m)
            .collect()
    }
}
