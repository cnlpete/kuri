/*
 * Copyright (C) 2022 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QGeoCoordinate>
#include <QObject>

namespace kuri {

struct TrackPoint {
    Q_GADGET

public:
    enum class Tag {
        Info,
        Track,
        Start,
        Pause,
        Resume,
        SectionCrossing,
        Stop,
    };
    Q_ENUM(Tag)

    TrackPoint() = default;
    TrackPoint(Tag tag, QGeoCoordinate coordinate);

    static void qmlRegisterTypes();

    Q_PROPERTY(Tag tag MEMBER tag)
    Q_PROPERTY(QGeoCoordinate coordinate MEMBER coordinate)

    Tag            tag {Tag::Info};
    QGeoCoordinate coordinate;
};

} // namespace kuri
