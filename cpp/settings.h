/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QObject>
#include <QSettings>

class Settings : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString hrmdevice READ hrmdevice WRITE setHrmdevice)
    Q_PROPERTY(bool recordPagePortrait READ recordPagePortrait WRITE setRecordPagePortrait)
    Q_PROPERTY(QString workoutType READ workoutType WRITE setWorkoutType)
    Q_PROPERTY(bool useHRMdevice READ useHRMdevice WRITE setUseHRMdevice)
    Q_PROPERTY(bool useHRMservice READ useHRMservice WRITE setUseHRMservice)
    Q_PROPERTY(bool disableScreenBlanking READ disableScreenBlanking WRITE setDisableScreenBlanking NOTIFY disableScreenBlankingChanged)
    Q_PROPERTY(bool showMapRecordPage READ showMapRecordPage WRITE setShowMapRecordPage)
    Q_PROPERTY(qreal positionAccuracyThreshold READ positionAccuracyThreshold WRITE setPositionAccuracyThreshold NOTIFY positionAccuracyThresholdChanged)
    Q_PROPERTY(bool enableLogFile READ enableLogFile WRITE setEnableLogFile)
    Q_PROPERTY(int displayMode READ displayMode WRITE setDisplayMode)
    Q_PROPERTY(QString valueFields READ valueFields WRITE setValueFields)
    Q_PROPERTY(bool enableAutosave READ enableAutosave WRITE setEnableAutosave)
    Q_PROPERTY(bool autoNightMode READ autoNightMode WRITE setAutoNightMode)
    Q_PROPERTY(int mapCenterMode READ mapCenterMode WRITE setMapCenterMode NOTIFY mapCenterModeChanged)
    Q_PROPERTY(QString mapStyle READ mapStyle WRITE setMapStyle)
    Q_PROPERTY(int mapCache READ mapCache WRITE setMapCache)
    Q_PROPERTY(QString valueCoverFields READ valueCoverFields WRITE setValueCoverFields)
    Q_PROPERTY(QString activityTypeFilter READ activityTypeFilter WRITE setActivityTypeFilter)
    Q_PROPERTY(int bluetoothType READ bluetoothType WRITE setBluetoothType)

    // strava specific settings
    Q_PROPERTY(quint64 stravaClientId READ stravaClientId NOTIFY stravaClientIdChanged)
    Q_PROPERTY(QString stravaClientSecret READ stravaClientSecret NOTIFY stravaClientSecretChanged)
    Q_PROPERTY(QString stravaAccessToken READ stravaAccessToken WRITE setStravaAccessToken NOTIFY stravaAccessTokenChanged)
    Q_PROPERTY(QString stravaRefreshToken READ stravaRefreshToken WRITE setStravaRefreshToken NOTIFY stravaRefreshTokenChanged)
    Q_PROPERTY(qint64 stravaExpirationTime READ stravaExpirationTime WRITE setStravaExpirationTime NOTIFY stravaExpirationTimeChanged)
    Q_PROPERTY(QString stravaUserName READ stravaUserName WRITE setStravaUserName NOTIFY stravaUserNameChanged)
    Q_PROPERTY(QString stravaCountry READ stravaCountry WRITE setStravaCountry NOTIFY stravaCountryChanged)

public:
    explicit Settings(QObject* parent = nullptr);

    QString hrmdevice() const;
    void    setHrmdevice(QString hrmdevice);

    bool recordPagePortrait() const;
    void setRecordPagePortrait(bool recordPagePortrait);

    QString workoutType() const;
    void    setWorkoutType(QString workoutType);

    bool useHRMdevice() const;
    void setUseHRMdevice(bool useHRMdevice);

    bool useHRMservice() const;
    void setUseHRMservice(bool useHRMdevice);

    bool disableScreenBlanking() const;
    void setDisableScreenBlanking(bool disableScreenBlanking);

    bool showMapRecordPage() const;
    void setShowMapRecordPage(bool showMapRecordPage);

    qreal positionAccuracyThreshold() const;
    void  setPositionAccuracyThreshold(qreal accuracyThreshold);

    bool enableLogFile() const;
    void setEnableLogFile(bool enableLogFile);

    int  displayMode() const;
    void setDisplayMode(int displayMode);

    QString valueFields() const;
    void    setValueFields(QString valueFields);

    bool enableAutosave() const;
    void setEnableAutosave(bool enableAutosave);

    bool autoNightMode() const;
    void setAutoNightMode(bool autoNightMode);

    int  mapCenterMode() const;
    void setMapCenterMode(int mapCenterMode);

    QString valueCoverFields() const;
    void    setValueCoverFields(QString valueCoverFields);

    QString mapStyle() const;
    void    setMapStyle(QString mapStyle);

    int  mapCache() const;
    void setMapCache(int mapCache);

    QString activityTypeFilter() const;
    void    setActivityTypeFilter(QString activityTypeFilter);

    // strava settings
    quint64 stravaClientId() const;
    QString stravaClientSecret() const;

    QString stravaAccessToken() const;
    void    setStravaAccessToken(QString accessToken);

    QString stravaRefreshToken() const;
    void    setStravaRefreshToken(QString accessToken);

    qint64 stravaExpirationTime() const;
    void   setStravaExpirationTime(qint64 expirationDate);

    QString stravaUserName() const;
    void    setStravaUserName(QString userName);

    QString stravaCountry() const;
    void    setStravaCountry(QString country);

    // bluetooth settings
    int  bluetoothType() const;
    void setBluetoothType(int stBluetoothType);


signals:
    void disableScreenBlankingChanged();
    void positionAccuracyThresholdChanged();
    void mapCenterModeChanged();
    void stravaClientIdChanged();
    void stravaClientSecretChanged();
    void stravaAccessTokenChanged();
    void stravaRefreshTokenChanged();
    void stravaExpirationTimeChanged();
    void stravaUserNameChanged();
    void stravaCountryChanged();

public slots:

private:
    QSettings* m_settings {nullptr};
};
