/*
 * Copyright (C) 2022 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "geo_position_info_source.hpp"

#include <QDebug>
#include <QSysInfo>

#include <random>

namespace kuri {

GeoPositionInfoSource::GeoPositionInfoSource()
    : QObject(nullptr) {
    auto hostName = QSysInfo::machineHostName();
    if (hostName == "SailfishEmul" || hostName.startsWith("SDKEmulator")) {
        qInfo() << "Emulator detected! Using simulator!";
        m_emulatorDetected = true;
        m_source.release();

        connect(&m_simulationTimer, &QTimer::timeout, this, &GeoPositionInfoSource::updateSimulatedPosition);

        auto millisecs = static_cast<uint16_t>(QDateTime::currentDateTimeUtc().time().msec());
        m_simulationTimer.start(1000 - millisecs + 50);
    } else if (m_source) {
        m_source->setPreferredPositioningMethods(QGeoPositionInfoSource::SatellitePositioningMethods);
        m_millisecondsUpdateIntervall = std::max(m_millisecondsUpdateIntervall, m_source->minimumUpdateInterval());
        m_source->setUpdateInterval(m_millisecondsUpdateIntervall);

        connect(m_source.get(), &QGeoPositionInfoSource::positionUpdated, this, &GeoPositionInfoSource::handlePositionUpdate);
    } else {
        qWarning() << "QGeoPositionInfoSource could not be created!";
    }

    m_accuracyOutdatedTimer.setInterval(m_millisecondsUpdateIntervall + 2500);
    connect(&m_accuracyOutdatedTimer, &QTimer::timeout, [&] {
        m_accuracy = -1.;
        emit accuracyChanged();
    });
}

void GeoPositionInfoSource::updateSimulatedPosition() {
    constexpr double latitudeCenter  = 52.5135;
    constexpr double longitudeCenter = 13.3603;
    constexpr double averageAccuracy = 10.;
    constexpr double radius          = 0.001;

    static std::random_device randomDevice;
    static std::mt19937       randomGenerator(randomDevice());

    static std::uniform_real_distribution<> altitudeDistribution(73.1, 82.7);
    static std::uniform_real_distribution<> accuracyJitterDistribution(-1.5, 1.5);

    static double angle = 0.;

    auto accuracyJitter   = accuracyJitterDistribution(randomGenerator);
    auto radiusWithJitter = radius * (1. + accuracyJitter / 100.);
    auto latitude         = latitudeCenter + sin(angle) * radiusWithJitter;
    auto longitude        = longitudeCenter + cos(angle) * radiusWithJitter;
    angle += 0.042;

    QGeoPositionInfo info;
    info.setCoordinate({latitude, longitude, altitudeDistribution(randomGenerator)});
    info.setTimestamp(QDateTime::currentDateTime());
    info.setAttribute(QGeoPositionInfo::HorizontalAccuracy, averageAccuracy + accuracyJitter);
    info.setAttribute(QGeoPositionInfo::VerticalAccuracy, averageAccuracy + accuracyJitter);
    // TODO also set the following attributes
    // - QGeoPositionInfo::Direction
    // - QGeoPositionInfo::GroundSpeed
    // - QGeoPositionInfo::VerticalSpeed
    // - QGeoPositionInfo::MagneticVariation

    auto currentMilliseconds      = static_cast<uint16_t>(QDateTime::currentDateTimeUtc().time().msec());
    auto millisecondsToFullSecond = 1000 - currentMilliseconds;
    auto millisecondsToSleep      = millisecondsToFullSecond + 50;
    if (millisecondsToSleep < 200) {
        // 200 is an arbitrary limit but it seems the last activation was way too early so add another second
        millisecondsToSleep += 1000;
    }
    m_simulationTimer.start(millisecondsToSleep);

    handlePositionUpdate(info);
}

void GeoPositionInfoSource::handlePositionUpdate(const QGeoPositionInfo& update) {
    m_currentPosition = update.coordinate();
    if (update.hasAttribute(QGeoPositionInfo::HorizontalAccuracy)) {
        m_accuracy = update.attribute(QGeoPositionInfo::HorizontalAccuracy);
    } else {
        m_accuracy = -1.;
    }
    emit currentPositionChanged();
    emit accuracyChanged();
    emit positionUpdated(update);

    // restart accuracy outdated timer
    m_accuracyOutdatedTimer.start();
}

void GeoPositionInfoSource::startUpdates() {
    if (m_source) {
        m_source->startUpdates();
    } else if (m_emulatorDetected) {
        m_simulationTimer.start();
    }

    m_accuracyOutdatedTimer.start();
}

void GeoPositionInfoSource::stopUpdates() {
    if (m_source) {
        m_source->stopUpdates();
    } else if (m_emulatorDetected) {
        m_simulationTimer.stop();
    }

    m_accuracyOutdatedTimer.stop();

    m_accuracy = -1.;
    emit accuracyChanged();
}

int GeoPositionInfoSource::updateInterval() const {
    return m_millisecondsUpdateIntervall;
}


qreal GeoPositionInfoSource::accuracy() const {
    return m_accuracy;
}

QGeoCoordinate GeoPositionInfoSource::currentPosition() const {
    return m_currentPosition;
}

} // namespace kuri
