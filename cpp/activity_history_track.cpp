/*
 * Copyright (C) 2023 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "activity_history_track.hpp"

#include "kuri-rs/ffi/events.h"

namespace kuri {

ActivityHistoryTrack::ActivityHistoryTrack(QObject* parent)
    : QObject(parent) {}

void ActivityHistoryTrack::setActivityHistoryRs(rs::ActivityHistory& history) {
    m_history = &history;
}

void ActivityHistoryTrack::load(QString activityId) {
    const auto id = activityId.toUtf8();
    m_history->load_track({id.data(), static_cast<size_t>(id.size())});
}

void ActivityHistoryTrack::discard() {
    m_history->discard_track();

    totalPointCountSet(0);

    currentElevationSet(0);
    currentDurationSet(0);
    currentHeartRateSet(0);
    currentDistanceSet(0);
    currentPaceSet(0);
}

void ActivityHistoryTrack::emitTrackPoints() {
    auto connectLateAvailabity    = [this] { this->connect(this, &ActivityHistoryTrack::availableChanged, this, &ActivityHistoryTrack::emitTrackPoints); };
    auto disconnectLateAvailabity = [this] { this->disconnect(this, &ActivityHistoryTrack::availableChanged, this, &ActivityHistoryTrack::emitTrackPoints); };

    if (!m_available) {
        disconnectLateAvailabity(); // just to prevent multiple signal connections
        connectLateAvailabity();
        return;
    }

    auto* track = m_history->track();
    if (track) {
        int64_t        lastTimestamp {0};
        QGeoCoordinate lastCoordinate;
        int64_t        lastTimestampWithValidCoordinate {0};

        const auto sectionCount    = track->section_count();
        const auto totalPointCount = track->total_point_count();
        prepareCache(sectionCount, totalPointCount);

        for (quint64 s = 0; s < sectionCount; ++s) {
            const auto sectionPointCount = track->section_point_count(s);

            if (sectionPointCount > 0) {
                auto m = m_history->track()->measurement_at(s, 0);

                m_cache.sectionPointCounts.push_back(sectionPointCount);
                m_cache.sectionInfo.push_back({m.timestamp});
                auto& sectionInfo = m_cache.sectionInfo.back();

                // emit point for marker
                if (s == 0) {
                    emit newTrackPoint({TrackPoint::Tag::Start, {m.latitude, m.longitude, m.altitude}});
                    lastTimestampWithValidCoordinate = 0;
                } else if (m.timestamp == lastTimestamp) {
                    emit newTrackPoint({TrackPoint::Tag::SectionCrossing, {m.latitude, m.longitude, m.altitude}});

                } else {
                    emit newTrackPoint({TrackPoint::Tag::Pause, lastCoordinate});
                    emit newTrackPoint({TrackPoint::Tag::Resume, {m.latitude, m.longitude, m.altitude}});
                    lastTimestampWithValidCoordinate = 0;
                }

                // emit all points in the section and populate cache
                for (quint64 i = 0; i < sectionPointCount; ++i) {
                    auto m        = m_history->track()->measurement_at(s, i);
                    lastTimestamp = m.timestamp;

                    if (!m.position_valid) { continue; }

                    auto currentCoordinate = QGeoCoordinate(m.latitude, m.longitude, m.altitude);
                    if (lastTimestampWithValidCoordinate != 0) {
                        // TODO in theory this should deliver the same results as statistics::great_circle_distance
                        // but to prevent deviations the same calculation method should be used
                        auto distance = currentCoordinate.distanceTo(lastCoordinate) * 1000.;
                        m_cache.distance.push_back(m_cache.distance.back() + distance);
                        sectionInfo.distanceInSection += distance;

                        auto timeDiff = static_cast<qreal>(m.timestamp - lastTimestampWithValidCoordinate);
                        auto pace     = (timeDiff / distance) * 1000000.;
                        m_cache.pace.push_back(pace);
                    } else if (m_cache.distance.empty()) {
                        m_cache.distance.push_back(0);
                        m_cache.pace.push_back(0.);
                    } else {
                        m_cache.distance.push_back(m_cache.distance.back());
                        m_cache.pace.push_back(m_cache.pace.back());
                    }

                    emit newTrackPoint({TrackPoint::Tag::Track, currentCoordinate});
                    lastCoordinate                   = currentCoordinate;
                    lastTimestampWithValidCoordinate = m.timestamp;
                }

                sectionInfo.durationInSection = lastTimestamp - sectionInfo.timestampSectionStart;
            }
        }

        emit newTrackPoint({TrackPoint::Tag::Stop, lastCoordinate});
        totalPointCountSet(totalPointCount);
        setCurrentIndex(0);

        // TODO simple moving average for data in m_cache.distance and m_cache.pace
    }

    disconnectLateAvailabity();
}

void ActivityHistoryTrack::setCurrentIndex(const quint64 index) {
    if (index >= m_totalPointCount) { return; }

    uint64_t section                    = 0;
    uint64_t indexInSection             = index;
    uint64_t accumulatedSectionDuration = 0;
    for (auto sectionPointCount : m_cache.sectionPointCounts) {
        if (indexInSection >= sectionPointCount) {
            indexInSection -= sectionPointCount;
            accumulatedSectionDuration += m_cache.sectionInfo[section].durationInSection;
            ++section;
        } else {
            auto m = m_history->track()->measurement_at(section, indexInSection);

            currentPositionSet(QGeoCoordinate(m.latitude, m.longitude, m.altitude));
            currentElevationSet(m.altitude);
            currentDurationSet(m.timestamp - m_cache.sectionInfo[section].timestampSectionStart + accumulatedSectionDuration);
            currentHeartRateSet(m.heart_rate);
            currentDistanceSet(m_cache.distance[index]);
            currentPaceSet(m_cache.pace[index]);

            break;
        }
    }
}

void ActivityHistoryTrack::prepareCache(size_t numberOfSections, size_t totalNumberOfPoints) {
    m_cache.sectionPointCounts.clear();
    m_cache.sectionInfo.clear();
    m_cache.distance.clear();
    m_cache.pace.clear();

    m_cache.sectionPointCounts.reserve(numberOfSections);
    m_cache.sectionInfo.reserve(numberOfSections);
    m_cache.distance.reserve(totalNumberOfPoints);
    m_cache.pace.reserve(totalNumberOfPoints);

    currentElevationSet(0);
    currentDurationSet(0);
    currentHeartRateSet(0);
    currentDistanceSet(0);
    currentPaceSet(0);
}

} // namespace kuri
