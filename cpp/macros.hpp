/*
 * Copyright (C) 2023 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

/// @brief This macro creates a 'Q_PROPERTY' and defines setter, getter methods and members,
/// e.g. 'KURI_RO_PROPERTY(bool, foo, false)' creates a 'Q_PROPERTY' for 'foo' with
/// `bool foo() const`/`void fooSet(bool)`/`void fooChanged()` and `m_foo {default}`;
/// the set method is private and cannot be accessed from outside the class
#define KURI_RO_PROPERTY(prop_type, prop_name, prop_default_value)           \
public:                                                                      \
    Q_PROPERTY(prop_type prop_name READ prop_name NOTIFY prop_name##Changed) \
    prop_type     prop_name() const;                                         \
    Q_SIGNAL void prop_name##Changed();                                      \
                                                                             \
private:                                                                     \
    void      prop_name##Set(prop_type value);                               \
    prop_type m_##prop_name { prop_default_value }


/// @brief This macro creates a 'Q_PROPERTY' and defines and implements default
/// setter, getter methods and members, e.g. 'KURI_RO_PROPERTY(bool, foo, false)' creates
/// a 'Q_PROPERTY' for 'foo' with `bool foo() const`/`void fooSet(bool)`/`void fooChanged()`
/// and `m_foo {default}`; the set method is private and cannot be accessed from outside the class
#define KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(prop_type, prop_name, prop_default_value) \
public:                                                                              \
    Q_PROPERTY(prop_type prop_name READ prop_name NOTIFY prop_name##Changed)         \
    prop_type     prop_name() const { return m_##prop_name; }                        \
    Q_SIGNAL void prop_name##Changed();                                              \
                                                                                     \
private:                                                                             \
    void prop_name##Set(prop_type value) {                                           \
        m_##prop_name = value;                                                       \
        emit prop_name##Changed();                                                   \
    }                                                                                \
    prop_type m_##prop_name { prop_default_value }


/// @brief This macro creates a 'Q_PROPERTY' and defines setter, getter methods and members,
/// e.g. 'KURI_RW_PROPERTY(bool, foo, false)' creates a 'Q_PROPERTY' for 'foo' with
/// `bool foo() const`/`void fooSet(bool)`/`void fooChanged()` and `m_foo {default}`
#define KURI_RW_PROPERTY(prop_type, prop_name, prop_default_value)                                \
public:                                                                                           \
    Q_PROPERTY(prop_type prop_name READ prop_name WRITE prop_name##Set NOTIFY prop_name##Changed) \
    prop_type     prop_name() const;                                                              \
    void          prop_name##Set(prop_type value);                                                \
    Q_SIGNAL void prop_name##Changed();                                                           \
                                                                                                  \
private:                                                                                          \
    prop_type m_##prop_name { prop_default_value }
