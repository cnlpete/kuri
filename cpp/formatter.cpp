/*
 * Copyright (C) 2021 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "formatter.hpp"

namespace kuri {

QString Formatter::secondsToHoursMinutesSeconds(const uint durationInSeconds) {
    uint seconds          = durationInSeconds % 60;
    uint remainingMinutes = (durationInSeconds - seconds) / 60;
    uint minutes          = remainingMinutes % 60;
    uint hours            = (remainingMinutes - minutes) / 60;

    return QString("%1:%2:%3").arg(hours, 2, 10, QLatin1Char('0')).arg(minutes, 2, 10, QLatin1Char('0')).arg(seconds, 2, 10, QLatin1Char('0'));
}

QString Formatter::secondsToHoursMinutes(const uint durationInSeconds) {
    uint seconds          = durationInSeconds % 60;
    uint remainingMinutes = (durationInSeconds - seconds) / 60;
    uint minutes          = remainingMinutes % 60;
    uint hours            = (remainingMinutes - minutes) / 60;

    return QString("%1:%2").arg(hours, 2, 10, QLatin1Char('0')).arg(minutes, 2, 10, QLatin1Char('0'));
}

QString Formatter::secondsToMinutesSeconds(const uint durationInSeconds) {
    uint seconds = durationInSeconds % 60;
    uint minutes = (durationInSeconds - seconds) / 60;

    return QString("%1:%2").arg(minutes, 2, 10, QLatin1Char('0')).arg(seconds, 2, 10, QLatin1Char('0'));
}

} // namespace kuri
