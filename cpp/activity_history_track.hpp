/*
 * Copyright (C) 2023 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "kuri-rs/rust/activity_history.rs.h"
#include "macros.hpp"
#include "track_point.hpp"

#include <QGeoCoordinate>
#include <QObject>

#include <vector>

namespace kuri {
class ActivityHistory;

class ActivityHistoryTrack : public QObject {
    Q_OBJECT

public:
    ActivityHistoryTrack(QObject* parent = nullptr);

    Q_INVOKABLE void load(QString activityId);
    Q_INVOKABLE void discard();
    Q_INVOKABLE void emitTrackPoints();

    Q_INVOKABLE void setCurrentIndex(quint64 index);

    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(bool, available, false);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, totalPointCount, 0);

    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(QGeoCoordinate, currentPosition, QGeoCoordinate {});
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, currentElevation, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, currentDuration, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint16, currentHeartRate, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, currentDistance, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(qreal, currentPace, 0.);

public:
signals:
    void newTrackPoint(TrackPoint point);

private:
    friend class ActivityHistory;
    void setActivityHistoryRs(rs::ActivityHistory& history);

    void prepareCache(size_t numberOfSections, size_t totalNumberOfPoints);

    struct SectionInfo {
        qint64  timestampSectionStart {0};
        quint64 durationInSection {0};
        quint64 distanceInSection {0};
    };

    struct Cache {
        std::vector<quint32>     sectionPointCounts;
        std::vector<SectionInfo> sectionInfo;
        std::vector<quint64>     distance;
        std::vector<qreal>       pace;
    };

private:
    rs::ActivityHistory* m_history {nullptr};
    Cache                m_cache;
};

} // namespace kuri
