/*
 * Copyright (C) 2023 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "activity_history.hpp"

namespace kuri {

ActivityHistory::ActivityHistory(Settings& settings, QObject* parent)
    : QSortFilterProxyModel(parent)
    ,m_activityTypeFilter(settings.activityTypeFilter())
    , m_settings(settings)
    {
    setSourceModel(&m_model);
    setFilterRole(static_cast<int>(ActivityHistoryModel::Roles::ActivityType));
    setFilterFixedString(m_activityTypeFilter.compare("allactivities") == 0 ? "" : m_activityTypeFilter);
    setSortRole(static_cast<int>(ActivityHistoryModel::Roles::Timestamp));
    constexpr int COLUMN {0};
    sort(COLUMN, Qt::DescendingOrder);

    m_track.setActivityHistoryRs(m_history);

    m_history.load_all_activities();

    connect(this, &ActivityHistory::activityTotalCountChanged, &m_model, &ActivityHistoryModel::updateActivityCache);
    m_model.updateActivityCache();
}

rs::ActivityHistory ActivityHistory::initActivityHistoryRs() {
    activitiesLoadingSet(false);

    auto  events  = rs::new_artifact_and_history_events();
    auto& aEvents = events.artifact;
    connect(aEvents.new_artifacts.get(), &ffi::EventVoid::signal, this, &ActivityHistory::processArtifacts, Qt::QueuedConnection);
    auto& hEvents = events.history;
    connect(hEvents.activities_loading.get(), &ffi::EventBool::signal, this, &ActivityHistory::activitiesLoadingSet, Qt::QueuedConnection);
    connect(hEvents.track_available.get(), &ffi::EventBool::signal, &this->m_track, &ActivityHistoryTrack::availableSet);
    connect(hEvents.activity_total_count_changed.get(), &ffi::EventU64::signal, this, &ActivityHistory::activityTotalCountSet);
    connect(hEvents.activity_summary_changed.get(), &ffi::EventVoid::signal, this, &ActivityHistory::propagateActivitySummaryChanges);
    return rs::new_activity_history(std::move(events));
}

void ActivityHistory::processArtifacts() {
    m_history.process_artifacts();
}

ActivityHistory::~ActivityHistory() {
    m_history.shutdown();
}

int ActivityHistory::mapToSourceRow(int row) {
    constexpr int COLUMN {0};
    auto          modelIndex = this->index(row, COLUMN);
    return mapToSource(modelIndex).row();
}

ActivityHistoryTrack& ActivityHistory::track() {
    return m_track;
}

void ActivityHistory::loadNewActivity(QString activityId) {
    const auto id = activityId.toUtf8();
    m_history.load_activity({id.data(), static_cast<size_t>(id.size())});
}

QString ActivityHistory::activityTypeFilter() const {
    return m_activityTypeFilter;
}

void ActivityHistory::activityTypeFilterSet(QString activityType) {
    setFilterFixedString(activityType.compare("allactivities") == 0 ? "" : activityType);
    m_activityTypeFilter = activityType;
    m_settings.setActivityTypeFilter(activityType);
    propagateActivitySummaryChanges();
}

void ActivityHistory::updateActivitySummary(QString activityId, int row, QString oldActivityType, QString newActivityType) {
    m_model.updateActivitySummary(activityId, mapToSourceRow(row), oldActivityType, newActivityType);
}

void ActivityHistory::saveActivityChanges(QString activityId, int row) {
    m_model.saveActivityChanges(activityId, mapToSourceRow(row));
}

void ActivityHistory::deleteActivity(QString activityId, int row) {
    m_model.removeActivity(activityId, mapToSourceRow(row));

    const auto id = activityId.toUtf8();
    m_history.delete_activity({id.data(), static_cast<size_t>(id.size())});
}

void ActivityHistory::propagateActivitySummaryChanges() {
    auto activityType = m_activityTypeFilter.toUtf8();
    const auto s = m_history.activity_summary({activityType.data(), static_cast<size_t>(activityType.size())});

    activityCountFromFilterSet(s.activity_count);
    activityDurationFromFilterSet(s.duration);
    activityDistanceFromFilterSet(s.distance);

    hasVariousActivityTypesSet(m_history.activity_type_count() > 1);
}
} // namespace kuri
