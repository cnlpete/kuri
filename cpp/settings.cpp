/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings.h"

#include "device.h"

Settings::Settings(QObject* parent)
    : QObject(parent) {
    m_settings = new QSettings("harbour-kuri", "harbour-kuri");
}

QString Settings::hrmdevice() const {
    return m_settings->value("hrm/hrmdevice", ",").toString();
}
void Settings::setHrmdevice(QString hrmdevice) {
    m_settings->setValue("hrm/hrmdevice", hrmdevice);
}

bool Settings::recordPagePortrait() const {
    return m_settings->value("generalsettings/recordpageportrait", true).toBool();
}
void Settings::setRecordPagePortrait(bool recordPagePortrait) {
    m_settings->setValue("generalsettings/recordpageportrait", recordPagePortrait);
}

QString Settings::workoutType() const {
    return m_settings->value("recordsettings/workoutType", "running").toString();
}
void Settings::setWorkoutType(QString workoutType) {
    m_settings->setValue("recordsettings/workoutType", workoutType);
}

bool Settings::useHRMdevice() const {
    return m_settings->value("hrm/useHRMdevice", false).toBool();
}
void Settings::setUseHRMdevice(bool useHRMdevice) {
    m_settings->setValue("hrm/useHRMdevice", useHRMdevice);
}

bool Settings::useHRMservice() const {
    return m_settings->value("hrm/useHRMservice", false).toBool();
}
void Settings::setUseHRMservice(bool useHRMservice) {
    m_settings->setValue("hrm/useHRMservice", useHRMservice);
}

bool Settings::disableScreenBlanking() const {
    return m_settings->value("recordsettings/disableScreenBlanking", false).toBool();
}
void Settings::setDisableScreenBlanking(bool disableScreenBlanking) {
    m_settings->setValue("recordsettings/disableScreenBlanking", disableScreenBlanking);
    emit disableScreenBlankingChanged();
}
bool Settings::showMapRecordPage() const {
    return m_settings->value("recordsettings/showMapRecordPage", true).toBool();
}
void Settings::setShowMapRecordPage(bool showMapRecordPage) {
    m_settings->setValue("recordsettings/showMapRecordPage", showMapRecordPage);
}
qreal Settings::positionAccuracyThreshold() const {
    return m_settings->value("positionSettings/accuracyThreshold", 40.).toReal();
}
void Settings::setPositionAccuracyThreshold(qreal accuracyThreshold) {
    m_settings->setValue("positionSettings/accuracyThreshold", accuracyThreshold);
    emit positionAccuracyThresholdChanged();
}
bool Settings::enableLogFile() const {
    return m_settings->value("generalsettings/enableLogFile", false).toBool();
}
void Settings::setEnableLogFile(bool enableLogFile) {
    m_settings->setValue("generalsettings/enableLogFile", enableLogFile);
}
int Settings::displayMode() const {
    return m_settings->value("recordsettings/displayMode", 0).toInt();
}
void Settings::setDisplayMode(int displayMode) {
    m_settings->setValue("recordsettings/displayMode", displayMode);
}
QString Settings::valueFields() const {
    return m_settings->value("recordsettings/valueFields", "3,4,1,2,7,8|5,6,1,2,7,8|5,6,1,2,7,8|3,4,1,2,7,8|5,6,1,2,7,8").toString();
}
void Settings::setValueFields(QString valueFields) {
    m_settings->setValue("recordsettings/valueFields", valueFields);
}
bool Settings::enableAutosave() const {
    return m_settings->value("generalsettings/enableAutosave", false).toBool();
}
void Settings::setEnableAutosave(bool enableAutosave) {
    m_settings->setValue("generalsettings/enableAutosave", enableAutosave);
}
bool Settings::autoNightMode() const {
    return m_settings->value("generalsettings/autoNightMode", true).toBool();
}
void Settings::setAutoNightMode(bool autoNightMode) {
    m_settings->setValue("generalsettings/autoNightMode", autoNightMode);
}
int Settings::mapCenterMode() const {
    return m_settings->value("mapsettings/mapCenterMode", 0).toInt();
}
void Settings::setMapCenterMode(int mapCenterMode) {
    m_settings->setValue("mapsettings/mapCenterMode", mapCenterMode);
    emit mapCenterModeChanged();
}

QString Settings::valueCoverFields() const {
    return m_settings->value("generalsettings/valueCoverFields", "10,8,3").toString();
}
void Settings::setValueCoverFields(QString valueCoverFields) {
    m_settings->setValue("generalsettings/valueCoverFields", valueCoverFields);
}

QString Settings::mapStyle() const {
    return m_settings->value("mapsettings/mapStyle", "mapbox://styles/mapbox/outdoors-v10").toString();
}
void Settings::setMapStyle(QString mapStyle) {
    m_settings->setValue("mapsettings/mapStyle", mapStyle);
}

int Settings::mapCache() const {
    return m_settings->value("mapsettings/mapCache", 50).toInt();
}
void Settings::setMapCache(int mapCache) {
    m_settings->setValue("mapsettings/mapCache", mapCache);
}

QString Settings::activityTypeFilter() const {
    return m_settings->value("generalsettings/activityTypeFilter", "allactivities").toString();
}
void Settings::setActivityTypeFilter(QString activityTypeFilter) {
    m_settings->setValue("generalsettings/activityTypeFilter", activityTypeFilter);
}

// strava settings
QByteArray encryptDecryptString(QByteArray token) {
    for (int i = 0; i < token.size() / 2; ++i) {
        const int  left  = 2 * i;
        const int  right = left + 1;
        const auto temp  = token.at(left);
        token[left]      = token[right];
        token[right]     = temp;
    }
    return token;
}

quint64 Settings::stravaClientId() const {
    // NOTE: Please don't reuse this strava client ID for your app if you copy this code but create a separate account
    // https://developers.strava.com/docs/getting-started/#account
    // https://developers.strava.com/docs/authentication/
    // https://developers.strava.com/docs/reference/
    return 75580;
}
QString Settings::stravaClientSecret() const {
    return QString(encryptDecryptString("55d77b7326d6e26ae64ef9e1ffe2e2cde20fd674"));
}

QString Settings::stravaAccessToken() const {
    return encryptDecryptString(m_settings->value("strava/accessToken").toString().toLatin1());
}
void Settings::setStravaAccessToken(QString accessToken) {
    m_settings->setValue("strava/accessToken", encryptDecryptString(accessToken.toLatin1()));
    emit stravaAccessTokenChanged();
}

QString Settings::stravaRefreshToken() const {
    return encryptDecryptString(m_settings->value("strava/refreshToken").toString().toLatin1());
}
void Settings::setStravaRefreshToken(QString accessToken) {
    m_settings->setValue("strava/refreshToken", encryptDecryptString(accessToken.toLatin1()));
    emit stravaRefreshTokenChanged();
}

qint64 Settings::stravaExpirationTime() const {
    return qvariant_cast<qint64>(m_settings->value("strava/expirationTime", 0));
}
void Settings::setStravaExpirationTime(qint64 expirationDate) {
    m_settings->setValue("strava/expirationTime", expirationDate);
    emit stravaExpirationTimeChanged();
}

QString Settings::stravaUserName() const {
    return m_settings->value("strava/userName").toString();
}
void Settings::setStravaUserName(QString userName) {
    m_settings->setValue("strava/userName", userName);
    emit stravaUserNameChanged();
}

QString Settings::stravaCountry() const {
    return m_settings->value("strava/country").toString();
}
void Settings::setStravaCountry(QString country) {
    m_settings->setValue("strava/country", country);
    emit stravaCountryChanged();
}

// bluetooth settings
int Settings::bluetoothType() const {
    return m_settings->value("hrm/bluetoothtype", Device::BLEPUBLIC).toInt();
}
void Settings::setBluetoothType(int stBluetoothType) {
    m_settings->setValue("hrm/bluetoothtype", stBluetoothType);
}
